/**
@file move.h
@author Samson Grice
@version 1.0
@date Mon 04 May 2020 16:12:31 UTC
@brief the header file for moves.c
*/

#ifndef __MOVE_H__
#define __MOVE_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int** p;
    int score;
}s_score;

s_score slide(int ** p, int direction);

int ** copy_tab(int ** p);

#endif
