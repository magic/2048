/**
@file move.c
@author Samson Grice
@version 1.0
@date Mon 04 May 2020 16:03:06 UTC
@brief file holding the move functions
*/

#include "move.h"
#include "2048.h"

/**
 * \fn copy_tab
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that copies the current table
 *
 *
 * \param argc : 1
 * \param argv : the table to copy
 * \return the copied table
 *
 * \remarks
 */

int ** copy_tab(int ** p){
    int ** c = creePlateau();
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            c[i][j] = p[i][j];
        }
    }
    return c;
}

/**
 * \fn rotate_tab_clockwise
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief rotates the table clockwise
 *
 *
 * \param argc : 1
 * \param argv : the table
 * \return the rotated table
 *
 * \remarks
 */

int ** rotate_tab_clockwise(int ** p){
    int ** c = creePlateau();
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            c[i][j] = p[3-j][i];
        }
    }
    free2(p);
    return c;
}

/**
 * \fn rotate_tab_anti_clockwise
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief rotates the table anti-clockwise
 *
 *
 * \param argc : 1
 * \param argv : the table
 * \return the rotated table
 *
 * \remarks
 */

int ** rotate_tab_anti_clockwise(int ** p){
    p = rotate_tab_clockwise(p);
    p = rotate_tab_clockwise(p);
    p = rotate_tab_clockwise(p);
    return p;
}

/**
 * \fn flip_vertical
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief flips the table verticaly
 *
 *
 * \param argc : 1
 * \param argv : the table
 * \return the flipped table
 *
 * \remarks
 */

int ** flip_vertical(int ** p){
    int ** c = creePlateau() /* copy_tab(p)*/ ;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            c[3-i][j] = p[i][j];
        }
    }
    //free2(p);
    return c;
}

/**
 * \fn slide_down
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that slides the table down
 *
 *
 * \param argc : 1
 * \param argv : the table to slide
 * \return the slided table
 *
 * \remarks
 */

s_score slide_down(int ** p){
    int swaps = 0, temp;
    s_score retour;
    retour.score = 0;
    int i,j;
    for (j=0; j<4;j++){
        do{
            swaps = 0;
            for (i=0; i<3;i++){
                if (p[i][j] != 0 && p[i+1][j] == 0){
                    temp = p[i][j];
                    p[i][j] = p[i+1][j];
                    p[i+1][j] = temp;

                    swaps++;
                }
            }
        }while (swaps);
    }
    // seperate in two function


    for (j=0; j<4; j++){
        for (i=2; i>=0;i--){
            if ((p[i][j] == p[i+1][j])){
                retour.score = retour.score + p[i][j] + p[i+1][j];
                p[i+1][j] = p[i][j] + p[i+1][j];
                p[i][j] = 0;
                int k;
                for (k=i; k>0;k--){
                    p[k][j] = p[k-1][j];
                }
                p[0][j] = 0;
            }
        }
    }
    retour.p = p;
    return retour;
}

/**
 * \fn slide_right
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that slides the table right
 *
 *
 * \param argc : 1
 * \param argv : the table to slide
 * \return the slided table
 *
 * \remarks
 */

s_score slide_right(int ** p){
    s_score retour;
    p = rotate_tab_clockwise(p);
    retour = slide_down(p);
    retour.p = rotate_tab_anti_clockwise(retour.p);
    return retour;
}

/**
 * \fn slide_left
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that slides the table left
 *
 *
 * \param argc : 1
 * \param argv : the table to slide
 * \return the slided table
 *
 * \remarks
 */

s_score slide_left(int ** p){
    s_score retour;
    p = rotate_tab_anti_clockwise(p);
    retour = slide_down(p);
    retour.p = rotate_tab_clockwise(retour.p);
    return retour;
}

/**
 * \fn slide_up
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that slides the table up
 *
 *
 * \param argc : 1
 * \param argv : the table to slide
 * \return the slided table
 *
 * \remarks
 */

s_score slide_up(int ** p){
    s_score retour;
    p = flip_vertical(p);
    retour = slide_down(p);
    retour.p = flip_vertical(retour.p);
    return retour;
}

/**
 * \fn slide
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that slides the table in function of the direction
 *
 *
 * \param argc : 2
 * \param argv : the table to slide
 * \param argv : the direction to slide
 * \return the slided table
 *
 * \remarks
 */

s_score slide(int ** p, int direction){
    /*
    2 - down
    4 - left
    8 - up
    6 - right
    */
    s_score retour;
    switch (direction){
        case 2:
            retour = slide_down(p);
            break;
        case 8:
            retour = slide_up(p);
            break;
        case 4:
            retour = slide_left(p);
            break;
        case 6:
            retour = slide_right(p);
            break;
        default:
            break;
    }

    return retour;
}
