#include "hard.h"

#define INF INT_MAX

int ** get_hard_move(int ** p, int move){
    int i,j;
    int max = 0;
    int num = 2;
    int mi = -1, mj = -1;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] == 0){
                switch (move){
                    case 8:
                        if (i>0){
                            if (p[i-1][j] > max){
                                max = p[i-1][j];
                                mi = i;
                                mj = j;
                                if (p[i-1][j] == 2){
                                    num = 4;
                                }
                            }
                        }
                        break;
                    case 6:
                        if (j<3){
                            if (p[i][j+1] > max){
                                max = p[i][j+1];
                                mi = i;
                                mj = j;
                                if (p[i][j+1] == 2){
                                    num = 4;
                                }
                            }
                        }
                        break;
                    case 2:
                        if (i<3){
                            if (p[i+1][j] > max){
                                max = p[i+1][j];
                                mi = i;
                                mj = j;
                                if (p[i+1][j] == 2){
                                    num = 4;
                                }
                            }
                        }
                        break;
                    case 4:
                        if (j>0){
                            if (p[i][j-1] > max){
                                max = p[i][j-1];
                                mi = i;
                                mj = j;
                                if (p[i][j-1] == 2){
                                    num = 4;
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    if (mi != -1){
        p[mi][mj] = num;
    }
    return p;
}
