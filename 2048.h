/*! \file 2048.h
 *  \author Amandine Legrand
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief contient les explications des fonctions qui servent � programmer le jeu 2048
 *
 *
 */

#ifndef __2048_H__
#define __2048_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "move.h"

struct jeu_save{
    char *name;
    int score;
    int ** p;
};

/**
 * \fn int saisieValeur(const char* texte)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date Fri Feb  7 10:23:22 2020
 *
 *  \brief Fonction qui saisie et v�rifie une valeur entr�e par l'utilisateur
 *
 *
 * \param argc : 1
 * \param argv : texte, le texte � afficher � l'utilisateur
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

int saisieValeur(const char* texte);

/**
 * \fn void freeTab(int** tab, int n)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date Mon Dec 16 16:17:46 2019
 *
 *  \brief Lib�re l'espace m�moire d'un tableau � 2 dimensions
 *
 *
 * \param argc : 2
 * \param argv : tab tableau � 2D, n le nb de lignes et nb de colonnes du tableau
 * \remarks
 */

void freeTab(int** tab, int n);

/**
 * \fn int** creePlateau(void)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 07/04/2020
 *
 *  \brief Fonction qui cr�e le plateau de traverse
 *
 *
 * \param argc : 0
 * \param argv :
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

 int** creePlateau(void);

/**
 * \fn int** initialisePlateau(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief Fonction qui initialise le plateau de 2048
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

int** initialisePlateau(int** p);

/**
 * \fn void affichePlateau(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief Fonction qui affiche le plateau de 2048
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

void affichePlateau(int** p);

/**
 * \fn int** apparition(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief Fonction qui fait appra�tre un chiffre 2 ou 4 sur une case au hasard
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048 et le score
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks la probabilit� pour avoir un chiffre 2 est plus grande que celle pour avoir un chiffre 4
 */

int** apparition(int** p);

/**
 * \fn s_score jeu(int ** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief Fonction qui demande ce que veut jouer le joueur et l'ex�cute
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

s_score jeu(s_score retour);

/**
 * \fn void tour(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief Fonction qui prend le tour du joueur
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks
 */

void tour(int** p);

/**
 * \fn int gagne(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 12/05/2020
 *
 *  \brief Fonction qui v�rifie si un joueur a gagn�
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks renvoie 1 si le joueur a gagn�, 0 sinon
 */

int gagne(int** p);

/**
 * \fn int plateauPlein(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 12/05/2020
 *
 *  \brief Fonction qui v�rifie si le joueur a perdu (le plateau est plein)
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks renvoie 1 si le plateau est plein, 0 sinon
 */

int plateauPlein(int** p);

/**
 * \fn int plateauPlein2(int** p)
 *  \author Legrand Amandine <legrandama@eisti.eu>
 *  \version 0.1
 *  \date 12/05/2020
 *
 *  \brief Fonction qui v�rifie si le joueur a perdu (le plateau est plein)
 *
 *
 * \param argc : 1
 * \param argv : p le plateau de 2048
 *�\return 0 : le programme doit se terminer normalement
 *
 * \remarks renvoie 1 si le plateau est plein, 0 sinon
 */

int plateauPlein2(int** p);

/**
 * \fn void free2(int ** c)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 22/05/2020
 *
 *  \brief Fonction that frees the table
 *
 *
 * \param argc : 1
 * \param argv : c the grid 2048
 */

void free2(int ** c);

/**
 * \fn int saveGame(int ** p, int score)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 22/05/2020
 *
 *  \brief Function that saves the game
 *
 *
 * \param argc : 1
 * \param argv : c the grid 2048
 * \param argc : 2
 * \param argv : the score of the game
 *
 * \return 0
 */

int saveGame(int ** p, int score);

/**
 * \fn s_score openGame()
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 23/05/2020
 *
 *  \brief Fonction that opens the game
 */

s_score openGame();

#endif // __2048_H__
