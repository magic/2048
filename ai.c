#include "ai.h"

/**
 * \fn deal_with_score
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that deals with the struct s_score when it is returned from other functions
 *
 *
 * \param argc : 1
 * \param argv : the s_score of the game
 * \return the table stored in s_score
 *
 * \remarks
 */

int ** deal_with_score(s_score res){
    return res.p;
}

/**
 * \fn same_tab
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that determines weather two tables are the same
 *
 *
 * \param argc : 2
 * \param argv : table 1
 * \param argv : table 2
 * \return 1 if a == b or 0 if a != b
 *
 * \remarks
 */

int same_tab(int ** a, int ** b){
    int res = 1;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (a[i][j] != b[i][j]){
                res = 0;
            }
        }
    }
    return res;
}

/**
 * \fn continue
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that determines if the game is still playable
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return 1 if you can continue to play or 0 if no moves are available
 *
 * \remarks
 */

int contiue(int ** p){
    int res = 0;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] == 0){
                res = 1;
            }
        }
    }
    for (i=0; i<3;i++){
        for (j=0; j<4;j++){
            if (p[i][j] == p[i+1][j] || p[j][i] == p[j][i+1]){
                res = 1;
            }
        }
    }
    if (res == 0){
    }
    return res;
}

/**
 * \fn big
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that returns the largest number in the grid
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the largest number in the grid
 *
 * \remarks
 */

int big(int ** p){
    int res = 0;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] > res){
                res = p[i][j];
            }
        }
    }
    return res;
}

/**
 * \fn edge
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on where the largest numbers are positioned
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int edge(int ** p){
    int limit = 32;
    int high = big(p);
    int score = 0;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] > limit && (i == 0 || j == 0 || i == 3 || j == 3)){
                if (p[i][j] == high && ((i == 0 && j == 0) || (i == 0 && j == 3) || (i == 3 && j == 0) || (i == 3 && j == 3))){
                    score += p[i][j] * p[i][j];
                }else if (p[i][j] != high && ((i == 0 && j == 0) || (i == 0 && j == 3) || (i == 3 && j == 0) || (i == 3 && j == 3))){
                    score -= high;
                }else{
                    score += p[i][j];
                }
            }else if (p[i][j] > limit){
                score -= (int)p[i][j];
            }else if (p[i][j] < limit && (i == 0 || j == 0 || i == 3 || j == 3)){
                //score -= p[i][j] * limit;
            }
        }
    }
    return score;
}

/**
 * \fn zero
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on the number of empty spots
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int zero(int ** p){
    int zeros = 0;
    int high = 0;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] == 0){
                zeros++;
            }else if (p[i][j] > high){
                high = p[i][j];
            }
        }
    }
    return (int)(zeros);
}

/**
 * \fn rows
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on the monotonicity of the rows
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int rows(int ** p){
    int high = big(p);
    int score = 0;
    int i;
    for (i=0; i<4;i++){
        if ((p[0][i] >= p[1][i] && p[1][i] >= p[2][i] && p[2][i] >= p[3][i])/* || (p[0][i] <= p[1][i] && p[1][i] <= p[2][i] && p[2][i] <= p[3][i])*/){
            if (p[0][i] > p[3][i]){
                score -= p[0][i];
            }else{
                score -= p[3][i];
            }
        }/*else if (((p[0][i] >= p[1][i] && p[1][i] >= p[2][i]) || (p[1][i] >= p[2][i] && p[2][i] >= p[3][i])) || ((p[0][i] <= p[1][i] && p[1][i] <= p[2][i]) || (p[1][i] <= p[2][i] && p[2][i] <= p[3][i]))){
            score += 10;
        }*/else{
            score += high;
        }
    }
    return score;
}

/**
 * \fn cols
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on the monotonicity of the cols
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int cols(int ** p){
    int high = big(p);
    int score = 0;
    int i;
    for (i=0; i<4;i++){
        if ((p[i][0] <= p[i][1] && p[i][1] <= p[i][2] && p[i][2] <= p[i][3])/* || (p[i][0] >= p[i][1] && p[i][1] >= p[i][2] && p[i][2] >= p[i][3])*/){
            if (p[i][3] > p[i][0]){
                score -= p[i][3];
            }else{
                score -= p[i][0];
            }

        }/*else if (((p[i][0] >= p[i][1] && p[i][1] >= p[i][2]) || (p[i][1] >= p[i][2] && p[i][2] >= p[i][3])) || ((p[i][0] <= p[i][1] && p[i][1] <= p[i][2]) || (p[i][1] <= p[i][2] && p[i][2] <= p[i][3]))){
            score += 10;
        }*/else{
            score += high;
        }
    }
    return score;
}

/**
 * \fn mono
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on the monotonicity
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int mono(int ** p){
    int score = cols(p);
    score += rows(p);

    return score;
}

/**
 * \fn merg
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table on the number of available merges
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int merg(int ** p){
     int score = 0;
     int i,j;
     for (i=0; i<3;i++){
         for (j=0; j<4;j++){
            if (p[i][j] == p[i+1][j]){
                score += p[i][j];
            }else if (p[j][i] == p[j][i+1]){
                score += p[j][i];
            }
         }
     }
     return score;
}

/**
 * \fn evaluate
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that scores the current table
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the score
 *
 * \remarks
 */

int evaluate(int ** p){
    int high = big(p);
    int count = 0;
    int i,j;
    for (i=0; i<4;i++){
        for (j=0; j<4;j++){
            if (p[i][j] >= high/4){
                count++;
            }
        }
    }

    int score = 0;
    int ed, ze, mo, me;
    if (contiue(p)){
        ed = 1 * edge(p);

        ze = zero(p);

        mo = -1 * mono(p);

        me = merg(p);

        //score *= high;

        me = me * (int)((16*1)/(ze+1));

        ze *= high;

        score = ed + ze + mo + me;

    }else{
        score = -INF/10000;
    }

    return score;
}

/**
 * \fn get_move_score
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a recursive function (exspectimax) that determines the move with the highest score
 *
 *
 * \param argc : 3
 * \param argv : table
 * \param argv : max depth (max 4 or too slow || 2 default)
 * \param argv : the turn (random number or ai move)
 * \return the max score
 *
 * \remarks
 */

int get_move_score(int ** c, int depth, int ismax){
    int ** p = copy_tab(c);
    int score = 0;
    int count = 0;
    int best_score;
    int moves[4] = {8,6,2,4};

    if (depth == 0 || !contiue(c)){
        best_score = evaluate(c);
    }else{
        if (ismax){
            best_score = -INF;

            int i;
            for (i=0; i<4;i++){
                free2(p);
                p = copy_tab(c);
                c = deal_with_score(slide(c, moves[i]));
                if (!same_tab(c, p)){
                    score = get_move_score(c, depth-1, (ismax+1)%2);
                }else{
                    score = -INF;
                }
                free2(c);
                c = copy_tab(p);
                if (score > best_score){
                    best_score = score;
                }
            }
        }else{
            best_score = 0;
            int i,j;
            for (i=0; i<4;i++){
                for (j=0; j<4;j++){
                    if (c[i][j] == 0){
                        c[i][j] = 2;
                        best_score += (int)(0.9f * (float)get_move_score(c, depth-1, (ismax+1)%2));
                        c[i][j] = 4;
                        best_score += (int)(0.1f * (float)get_move_score(c, depth-1, (ismax+1)%2));
                        c[i][j] = 0;
                        count += 2;
                    }
                }
            }
            best_score /= count;
        }
    }
    free2(p);
    return best_score;
}

/**
 * \fn ai_get_move
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 28/04/2020
 *
 * \brief a function that determines the best move
 *
 *
 * \param argc : 1
 * \param argv : table
 * \return the move
 *
 * \remarks
 */

int ai_get_move(int ** p){
    int ** c = copy_tab(p);
    int move;
    int score = 0;
    int best_score = -INF;

    int moves[4] = {8,6,2,4};

    int i;
    for (i=0; i<4;i++){
        free2(c);
        c = copy_tab(p);
        p = deal_with_score(slide(p, moves[i]));
        if (!same_tab(c, p)){
            //score = evaluate(p);
            score = get_move_score(p, 2, 0);
        }else{
            score = -INF;
        }
        free2(p);
        p = copy_tab(c);
        if (score > best_score){
            move = moves[i];
            best_score = score;
        }

    }
    free2(c);
    return move;
}
