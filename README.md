![Alt Text](https://gitlab.etude.cy-tech.fr/magic/2048/-/raw/master/2048.gif)

# 2048

This is the readme of our 2048 project. It explains how to compile the code. The program requires SDL2 and SDL2_ttf in order to run.

## Compiling

### Linux:

After having downloaded the source use the command make and the Makefile should automatically create the executable.

It requries SDL2 and SDL2_ttf which are installed with the commands: 

 - ```sudo apt-get install libsdl2-dev```
 - ```sudo apt-get install libsdl2-dev-ttf```

If these don't work try downloading the 
[source](https://www.libsdl.org/download-2.0.php#source) 
and move the folder to the correct location. Depending on the version that you install, it might be nessesary to change the SDL2_ttf-2.0.15 folder name in SDL_2048.c line 6. 
If all else fails we followed 
[this](http://lazyfoo.net/tutorials/SDL/01_hello_SDL/linux/index.php)
 tutorial here.

SDL2_ttf: 
[source](https://www.libsdl.org/projects/SDL_ttf/) 
[tutorial](http://lazyfoo.net/tutorials/SDL/06_extension_libraries_and_loading_other_image_formats/linux/index.php)

### Windows
Most of the group doesn't have access to a linux based machine so it was mainly compilled using CodeBlocks following these tutorials: 
[SDL](http://lazyfoo.net/tutorials/SDL/01_hello_SDL/windows/codeblocks/index.php), [SDL_ttf](http://lazyfoo.net/tutorials/SDL/06_extension_libraries_and_loading_other_image_formats/windows/codeblocks/index.php)

The code should determine which os ( except mac, not been tested ) you are using to find the right location for the libaries.
## Usage

Hopefully after compiling you should have a executable program, all that is left is too run the program and play.
