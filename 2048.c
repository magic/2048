/*! \file 2048.c
 *  \author Amandine Legrand
 *  \version 0.1
 *  \date 05/05/2020
 *
 *  \brief programme le jeu 2048
 *
 *
 */

#include "2048.h"
#include "move.h"
#include "ai.h"
#include "hard.h"

/**
 * \fn char * int_to_string(int n, char * word)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 27/04/2020
 *
 *  \brief Fonction that converts an integer to string
 *
 *
 * \param argc : 2
 * \param argv : the integer to convert
 * \param argv : the address of the string
 * \return char * : the string
 *
 * \remarks
 */

char * int_to_string(int n, char * word){
    sprintf(word, "%d", n);
    return word;
}

void free2(int ** c)
{
    if (c != NULL){
        int i;
        for (i=0; i<4;i++){
            if (c[i] != NULL){
                free(c[i]);
                c[i] = NULL;
            }
        }
        free(c);
        c = NULL;
    }
}

int saisieValeur(const char* texte)
{
	int retour;
	int valeur;
	char caractereSuivant;
	printf("%s", texte);
	retour = scanf("%d", &valeur);
	caractereSuivant = getchar();
	if ((retour == 0) || (caractereSuivant == '.') || (caractereSuivant == ',')) {
		fprintf(stderr, "Probl�me de saisie utilisateur \n");
		exit(EXIT_FAILURE);
	}
	return(valeur);
}

void freeTab(int** tab, int n)
{
	int i;
	for (i=0; i<n; i++) {
		free(tab[i]);
	}
	free(tab);
}

int** creePlateau (void)
{
    int** plateau;
    int i;
    plateau = (int**) malloc(sizeof(int*)*4);
	if (plateau==NULL) {
		fprintf(stderr, "Probl�me d'allocation \n");
		exit(1);
	}
	for (i=0; i<4; i++) {
		plateau[i] = (int*) malloc(sizeof(int)*4);
		if (plateau[i]==NULL) {
			fprintf(stderr, "Probl�me d'allocation \n");
			exit(1);
		}
	}
	return(plateau);
}

int** initialisePlateau(int** p)
{
    int i2;
    int j2;
    int i1;
    int j1;
    for (i1=0; i1<4; i1++) {
        for (j1=0; j1<4; j1++) {
            p[i1][j1] = 0;
        }
    }
    i1 = rand()%4;
    j1 = rand()%4;
    p[i1][j1] = 2;
    i2 = i1;
    j2 = j1;
    while ((i1 == i2)&&(j1 == j2)) {
        i2 = rand()%4;
        j2 = rand()%4;
        p[i2][j2] = 2;
    }
    return(p);
}

void affichePlateau(int** p)
{
    system("cls");
    int i;
    int j;
    for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
            if (p[i][j] == 0) {
                printf("|     ");
            } else {
                printf("|  %d  ", p[i][j]);
            }
        }
        printf("| \n");
    }
    printf("\n");
}

int** apparition(int** p)
{
    int choixHasard;
    int i;
    int j;
    int i2;
    int j2;
    for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
            if (p[i][j] !=  0) {
                i2 = i;
                j2 = j;
            }
        }
    }
    choixHasard = rand()%10;
    while (p[i2][j2] != 0) {
        i2 = rand()%4;
        j2 = rand()%4;
    }
    if (choixHasard == 1) {
        p[i2][j2] = 4;
    } else {
        p[i2][j2] = 2;
    }
    return(p);
}

int gagne(int** p)
{
    int i;
    int j;
    int retour;
    retour = 0;
    for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
            if (p[i][j] == 2048) {
                retour = 1;
            }
        }
    }
    return(retour);
}

int plateauPlein2(int ** p)
{
    int retour;
    int i;
    int j;
    retour = 1;
    for (i=0; i<3; i++) {
        for (j=0; j<4; j++) {
            if (p[i][j] == p[i+1][j] || p[j][i] == p[j][i+1]){
                retour = 0;
            }
        }
    }
    return(retour);
}

int plateauPlein(int** p)
{
    int retour;
    int i;
    int j;
    retour = 1;
    for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
            if (p[i][j] == 0) {
                retour = 0;
            }
        }
    }
    return(retour);
}

/**
 * \fn char * getWord(FILE * fptr)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 22/04/2020
 *
 *  \brief Fonction that gets the next word in the file
 *
 *
 * \param argc : 1
 * \param argv : the file
 * \return char * : the word
 *
 * \remarks
 */

char * getWord(FILE * fptr){
	char * word = malloc(10);
	signed char c = fgetc(fptr);
	int index = 0;
	while (c != ':' && c != EOF && c != ',' && c != ']'){
        if (c != '\"'){
            word[index] = c;
            index++;
        }
        c = fgetc(fptr);
	}
	ungetc(c, fptr);
	word[index] = '\0';
	return word;
}

s_score openGame()
{
	FILE * fptr;
	fptr = fopen("games.json", "r");
	if (fptr == NULL){
		printf("could not open file \n");
		exit(1);
	}
	signed char c = fgetc(fptr);
	char * word;
	char * name;
	int score;
	int ** p = creePlateau();
	int index;
	while (c != EOF){
		if (c == '{' || c == ','){
			word = getWord(fptr);
			if (strcmp(word, "name") == 0){
                fgetc(fptr);
				name = getWord(fptr);
			}else if (strcmp(word, "score") == 0){
			    fgetc(fptr);
				name = getWord(fptr);
                score = strtol(name, NULL, 10);
			}else if (strcmp(word, "game") == 0){
			    fgetc(fptr);
			    for(index = 0; index < 16; index++){
			        c = fgetc(fptr);
			        name = getWord(fptr);
                    p[index/4][index%4] = strtol(name, NULL, 10);
			    }
			}
		}
		c = fgetc(fptr);
	}

	s_score res;
	res.p = p;
	res.score = score;
	return res;
}

/**
 * \fn int writeGame(FILE * fptr, int ** p)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 22/04/2020
 *
 *  \brief Fonction that writes the game to the file
 *
 *
 * \param argc : 2
 * \param argv : the file
 * \param argv : the game
 * \return 0
 *
 * \remarks
 */

int writeGame(FILE * fptr, int ** p)
{
    char * score = malloc(10);
    fputc('[', fptr);
	int i,j;
	for (i=0; i<4; i++) {
        for (j=0; j<4; j++) {
			score = int_to_string(p[i][j], score);
			fputs(score, fptr);
			if (i*4+j != 15){
				fputc(',', fptr);
			}
        }
	}
    fputc(']', fptr);
    return 0;
}

/**
 * \fn writeToFile(FILE * fptr, struct jeu_save game)
 *  \author Grice Samson <gricemagic@eisti.eu>
 *  \version 0.1
 *  \date 22/04/2020
 *
 *  \brief Fonction that writes the name, score, game to the file to save
 *
 *
 * \param argc : 2
 * \param argv : the file
 * \param argv : the struct containing the score, game
 * \return 0
 *
 * \remarks
 */

int writeToFile(FILE * fptr, struct jeu_save game)
{
    char * score = malloc(10);
    score = int_to_string(game.score, score);
    fputc('{', fptr);
    fputs("\"name\":", fptr);
    fputc('\"', fptr);
    fputs(game.name, fptr);
    fputc('\"', fptr);
    fputc(',', fptr);
    fputs("\"score\":", fptr);
    fputs(score, fptr);
    fputc(',', fptr);
    fputs("\"game\":", fptr);
    writeGame(fptr, game.p);
    fputc('}', fptr);
    return 0;
}

int saveGame(int ** p, int score)
{
    char *name = malloc(20);
    //printf("Please enter your name: ");
    //scanf(" %s", name);
    strcpy(name, "game");
    FILE *fptr = fopen("games.json", "w+");
    if (fptr == NULL){
        printf("Erooor opening file: %s \n", "games.json");
        exit(1);
    }
    struct jeu_save game = {name, score, p};
    writeToFile(fptr, game);
    fclose(fptr);
    free(fptr);
    return 0;
}

/* ---  Functions for the terminal game
s_score jeu(s_score retour)
{
    int ai = 1;
    int hard = 0;
    int score = retour.score;
    int choix;
    if (ai){
        choix = ai_get_move(retour.p);
    }else{
        printf("Taper 6 pour aller � droite \nTaper 4 pour aller � gauche \nTaper 2 pour aller en bas \nTaper 8 pour aller en haut \n");
        scanf("%d", &choix);
        while ((choix != 2)&&(choix!=4)&&(choix!=6)&&(choix!=8)&&(choix!=5)&&(choix!=0)) {
            printf("Erreur, saisissez � nouveau une valeur\n");
            scanf("%d", &choix);
        }
    }

    if (choix != 5 && choix != 0){
        retour = slide(retour.p, choix);
        retour.score += score;

        if (hard){
            retour.p = get_hard_move(retour.p, choix);
        }else{
            retour.p = apparition(retour.p);
        }
    }else if (choix == 5){
        printf("%d\n", retour.score);
        saveGame(retour.p, retour.score);
        printf("%d\n", retour.score);
    }else if (choix == 0){
		retour = openGame();
    }
    return(retour);
}

void tour(int** p)
{
    s_score str;
    str.p = p;
    while ((gagne(str.p) == 0) && (plateauPlein(str.p) == 0)) {
        str = jeu(str);
        affichePlateau(str.p);
        printf("score : %d \n", str.score);
    }
    printf("ended\n");
}
*/
