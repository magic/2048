#if defined(_WIN32) || defined(_WIN64)
#include <SDL.h>
#include <SDL_ttf.h>
#elif defined(__unix__) || defined(linux)
#include <SDL2/SDL.h>
#include <SDL2_ttf-2.0.15/SDL_ttf.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "ai.h"
#include "2048.h"
#include "hard.h"

//Screen dimension constants
const int SCREEN_WIDTH = 400;
const int SCREEN_HEIGHT = 400;

int init();

//The window we'll be rendering to
SDL_Window* window = NULL;

//The surface contained by the window
SDL_Surface* screen = NULL;

//The renderer to display text
SDL_Renderer * renderer = NULL;

TTF_Font *font = NULL;

SDL_Colour BLACK = {0,0,0,0};

//The menu image
SDL_Surface * menu_image = NULL;

//The tile image
SDL_Surface * tile_image = NULL;

//The font image
SDL_Surface * fond_image = NULL;

//The setting image
SDL_Surface * setting_image = NULL;

//The select image
SDL_Surface * select_image = NULL;

//The pause image
SDL_Surface * pause_image = NULL;

//The win image
SDL_Surface * win_image = NULL;

//The lose image
SDL_Surface * lose_image = NULL;

//The hard variable
int HARD = 0;
int THEME = 0;

/**
 * \fn init
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that initialises all the SDL requirements
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int init()
{
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		fprintf(stderr, "SDL could not initialize! SDL_INIT(): %s\n", SDL_GetError() );
		return 0;
	}

	//Initialize SDL_ttf
    if( TTF_Init() == -1 )
    {
        fprintf(stderr, "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
        return 0;
    }

    font = TTF_OpenFont("arial.ttf", 26);
    if (font == NULL){
        fprintf(stderr, "Font %s could not be found TTF_OpenFont: %s\n", "arial.ttf", TTF_GetError());
        return 0;
    }


    //Create window
    window = SDL_CreateWindow( "2048", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    if( window == NULL )
    {
        fprintf(stderr, "Window could not be created! SDL_CreateWindow(): %s\n", SDL_GetError() );
        return 0;
    }

    //Get window surface
    screen = SDL_GetWindowSurface( window );

    //Load splash image
	menu_image = SDL_LoadBMP( "2048.bmp" );
	if( menu_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "2048.bmp", SDL_GetError() );
		return 0;
	}
	tile_image = SDL_LoadBMP( "tiles0.bmp" );
	if( tile_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "tiles.bmp", SDL_GetError() );
		return 0;
	}
	fond_image = SDL_LoadBMP( "fond.bmp" );
	if( fond_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "fond.bmp", SDL_GetError() );
		return 0;
	}
	setting_image = SDL_LoadBMP( "settings_off.bmp" );
	if( setting_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "fond.bmp", SDL_GetError() );
		return 0;
	}
	select_image = SDL_LoadBMP( "selector.bmp" );
	if( select_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "fond.bmp", SDL_GetError() );
		return 0;
	}
	pause_image = SDL_LoadBMP( "pause.bmp" );
	if( pause_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "pause.bmp", SDL_GetError() );
		return 0;
	}
	win_image = SDL_LoadBMP( "win.bmp" );
	if( pause_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "win.bmp", SDL_GetError() );
		return 0;
	}
	lose_image = SDL_LoadBMP( "lose.bmp" );
	if( pause_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", "lose.bmp", SDL_GetError() );
		return 0;
	}

	return 1;
}

/**
 * \fn reload_window
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that changes the size of the window
 *
 *
 * \param argc : 2
 * \param argv : the new width
 * \param argv : the new height
 * \return 1
 *
 * \remarks
 */

int reload_window(int width, int height)
{
    //Create window
    SDL_SetWindowSize(window, width, height);

    //Get window surface
    SDL_FreeSurface(screen);
    screen = SDL_GetWindowSurface( window );
    return 1;
}

/**
 * \fn loadTiles
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that loads the theme from the theme number
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int loadTiles()
{
    char name[10];
    switch (THEME){
    case 0:
        strcpy(name, "tiles0.bmp");
        break;
    case 1:
        strcpy(name, "tiles1.bmp");
        break;
    case 2:
        strcpy(name, "tiles2.bmp");
        break;
    case 3:
        strcpy(name, "tiles3.bmp");
        break;
    case 4:
        strcpy(name, "tiles4.bmp");
        break;
    case 5:
        strcpy(name, "tiles5.bmp");
        break;
    case 6:
        strcpy(name, "tiles6.bmp");
        break;
    case 7:
        strcpy(name, "tiles7.bmp");
        break;
    default:
        strcpy(name, "tiles0.bmp");
        break;
    }
    SDL_FreeSurface(tile_image);
    tile_image = SDL_LoadBMP( name );
	if( tile_image == NULL )
	{
		printf( "Unable to load image %s! SDL Error: %s\n", name, SDL_GetError() );
		return 0;
	}
    return 0;
}

/**
 * \fn afficher
 * \author Morelli Candice <morellican@eisti.eu>
 * \version 0.2
 * \date 21/05/2020
 *
 * \brief une fonction qui affiche le tableau dans SDL
 *
 *
 * \param argc : 1
 * \param argv : le tableau
 * \return 0
 *
 * \remarks
 */

int afficher(int ** p)
{
    SDL_Rect fond_pos = {0,0,SCREEN_WIDTH, SCREEN_HEIGHT};
    SDL_BlitSurface( fond_image, NULL, screen, &fond_pos );
    int i,j;
    for (i=0; i<4; i++){
        for (j=0; j<4;j++){
            //printf("%d ", p[i][j]);
            SDL_Rect screen_pos = {i*100+5, j*100+5, 100-13, 100-13};
            SDL_Rect image_pos = {10,10,90,90};
            if (p[i][j] != 0){
                int index = (int)log2f(p[i][j]);
                image_pos.x = 100 * index+5;
            }
            SDL_BlitSurface(tile_image, &image_pos, screen, &screen_pos);
        }
    }
    return 0;
}

/**
 * \fn drawText
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that text to the screen
 *
 *
 * \param argc : 3
 * \param argv : the string to write
 * \param argv : the x coor
 * \param argv : the y coor
 * \return 1
 *
 * \remarks
 */

void drawText(char * text, int x, int y)
{
    SDL_Surface * text_sur = TTF_RenderText_Solid(font, text, BLACK);
    SDL_Rect text_rect = {x,y,text_sur->w,text_sur->h};
    SDL_BlitSurface(text_sur, NULL, screen, &text_rect);
}

/**
 * \fn buttons2
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that handles the mouse input for the setting screen
 *
 *
 * \param argc : 2
 * \param argv : the x coor of the mouse
 * \param argv : the y coor of the mouse
 * \return 0
 *
 * \remarks
 */

int buttons2(int x,int y)
{
    if (x > 146 && x < 256 && y > 349 && y < 393){
        return 1;
    }else if (x > 250 && x < 318 && y > 90 && y < 115){
        // change hard mode
        if (HARD){
            HARD = 0;
            SDL_FreeSurface(setting_image);
            setting_image = SDL_LoadBMP( "settings_off.bmp" );
            if( fond_image == NULL )
            {
                printf( "Unable to load image %s! SDL Error: %s\n", "settings_off.bmp", SDL_GetError() );
                return 0;
            }
        }else{
            HARD = 1;
            SDL_FreeSurface(setting_image);
            setting_image = SDL_LoadBMP( "settings_on.bmp" );
            if( fond_image == NULL )
            {
                printf( "Unable to load image %s! SDL Error: %s\n", "settings_on.bmp", SDL_GetError() );
                return 0;
            }
        }
    }else if (x > 250 && x < 250 + 15 * 8 && y > 150 && y < 165){
        THEME = (int)((x-250)/15);
        loadTiles();
    }
    return 0;
}

/**
 * \fn buttons3
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that handles the mouse input for the pause screen
 *
 *
 * \param argc : 4
 * \param argv : the x coor of the mouse
 * \param argv : the y coor of the mouse
 * \param argv : the game
 * \param argv : a value to change continue or not
 * \return 0
 *
 * \remarks
 */

s_score buttons3(int x,int y, s_score game, int * return_value)
{
    if (x > 120 && x < 270){
        if (y > 76 && y < 112){
            //printf("continue\n");
            *return_value = 1;
            return game;
        }else if (y > 140 && y < 172){
            //printf("save\n");
            saveGame(game.p, game.score);
            *return_value = 0;
        }else if (y > 197 && y < 228){
            //printf("open\n");
            game = openGame();
            *return_value = 1;
            return game;
        }else if (y > 329 && y < 374){
            //printf("quit\n");
            *return_value = 2;
            return game;
        }
    }
    return game;
}

/**
 * \fn pause
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the pause event
 *
 *
 * \param argc : 2
 * \param argv : the game
 * \param argv : a value to determine weather or not to continue
 * \return 0
 *
 * \remarks
 */

s_score pause(s_score game, int * return_value)
{
    reload_window(400,400);
    SDL_Event event;
    int quit = 0;
    int x,y;

    while (!quit){

        SDL_WaitEvent(&event);
        SDL_BlitSurface( pause_image, NULL, screen, NULL );

        switch (event.type){
        case SDL_QUIT:
            quit = 1;
            free2(game.p);
            game.p = NULL;
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            int value = 0;
            game = buttons3(x,y, game, &value);
            if (game.p == NULL){
                quit = 1;
            }
            if (value == 1){
                quit = 1;
            }else if (value == 2){
                *return_value = 2;
                quit = 1;
            }
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
            case SDLK_ESCAPE:
                return game;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        //Update the surface
        SDL_UpdateWindowSurface( window );
    }

    reload_window(500,400);
    return game;
}

/**
 * \fn end_game
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the end game event
 *
 *
 * \param argc : 2
 * \param argv : 1 if the player has won else 0
 * \param argv : the final score to show
 * \return 0
 *
 * \remarks
 */

int end_game(int won, int score)
{
    reload_window(400,400);
    SDL_Event event;
    int quit = 0;
    int x,y;
    char score_string[6];


    while (!quit){

        SDL_WaitEvent(&event);
        if (won){
        SDL_BlitSurface( win_image, NULL, screen, NULL );
        }else{
        SDL_BlitSurface( lose_image, NULL, screen, NULL );
        }
        sprintf(score_string, "%d", score);
        drawText("SCORE:", 150, 115);
        drawText(score_string, 150, 150);

        switch (event.type){
        case SDL_QUIT:
            quit = 1;
            SDL_Quit();
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            if (x > 120 && x < 270 && y > 76 && y < 112){
                reload_window(500,400);
                if (won){
                    return 2;
                }else{
                    return 0;
                }
            }else if (x > 120 && x < 270 && y > 329 && y < 374){
                return 0;
            }
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
            case SDLK_ESCAPE:
                return 0;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        //Update the surface
        SDL_UpdateWindowSurface( window );
    }
    return 0;
}

/**
 * \fn play
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the game play for human players
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int play()
{
    int ** p = creePlateau();
    p = initialisePlateau(p);
    p = apparition(p);

    SDL_Event event;
    int x,y, quit = 0;

    s_score str;
    str.score = 0;
    str.p = p;
    char score_string[6];

    int score=0,move, value = 0, win_lose_continue = 1;

    while (win_lose_continue && !quit){
        move = 0;

        SDL_WaitEvent(&event);

        switch (event.type){
        case SDL_QUIT:
            SDL_Quit();
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
            case SDLK_ESCAPE:
                value = 0;
                str = pause(str, &value);
                if (str.p == NULL){
                    SDL_Quit();
                }
                if (value == 2){
                    free2(str.p);
                    return 0;
                }
                break;
            case SDLK_UP:
                move = 4;
                break;
            case SDLK_DOWN:
                move = 6;
                break;
            case SDLK_LEFT:
                move = 8;
                break;
            case SDLK_RIGHT:
                move = 2;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        if (move){
            score = str.score;
            str = slide(str.p, move);
            str.score += score;
            if (plateauPlein(str.p) == 0){
                if (HARD){
                    str.p = get_hard_move(str.p, move);
                }else{
                    str.p = apparition(str.p);
                }
            }
        }

        afficher(str.p);
        sprintf(score_string, "%d", str.score);
        drawText("SCORE:", 400, 5);
        drawText(score_string, 400, 40);
        //Update the surface
        SDL_UpdateWindowSurface( window );

        if (gagne(str.p)){
            // youve won
            if (win_lose_continue == 2){
                //youve already won and want to continue
            }else{
                win_lose_continue = end_game(gagne(str.p), str.score);
            }
        }
        if ((plateauPlein2(str.p)) && (plateauPlein(str.p))){
            // you loose
            win_lose_continue = end_game(gagne(str.p), str.score);
            win_lose_continue = 0;
        }
    }
    //printf("score : %d \n", str.score);
    free2(str.p);
    free2(p);;

    return 0;
}

/**
 * \fn play_ai
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the game play for the ai
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int play_ai()
{
    int ** p = creePlateau();
    p = initialisePlateau(p);
    p = apparition(p);

    SDL_Event event;
    int x,y;

    s_score str;
    str.score = 0;
    str.p = p;
    int score=0,move;
    char score_string[6];
    int win_lose_continue = 1, quit = 0;

    while (win_lose_continue && !quit){
        move = 0;
        SDL_PollEvent(&event);
        switch (event.type){
        case SDL_QUIT:
            SDL_Quit();
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
            case SDLK_ESCAPE:
                return 0;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }

        move = ai_get_move(str.p);

        if (move){
            score = str.score;
            str = slide(str.p, move);
            str.score += score;
            if (plateauPlein(str.p) == 0){
                if (HARD){
                    str.p = get_hard_move(str.p, move);
                }else{
                    str.p = apparition(str.p);
                }
            }
        }

        afficher(str.p);
        sprintf(score_string, "%d", str.score);
        drawText("SCORE:", 400, 5);
        drawText(score_string, 400, 40);
        //Update the surface
        SDL_UpdateWindowSurface( window );

        if (gagne(str.p)){
            // youve won
            if (win_lose_continue == 2){
                //youve already won and want to continue
            }else{
                win_lose_continue = end_game(gagne(str.p), str.score);
            }
        }
        if ((plateauPlein2(str.p)) && (plateauPlein(str.p))){
            // you loose
            win_lose_continue = end_game(gagne(str.p), str.score);
            win_lose_continue = 0;
        }
    }
    free2(str.p);
    free2(p);

    return 0;
}

/**
 * \fn settings
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the settings menu
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int setting()
{
    SDL_Event event;
    int back = 0;
    int x,y;

    while (!back){

        SDL_WaitEvent(&event);
        SDL_BlitSurface( setting_image, NULL, screen, NULL );

        switch (event.type){
        case SDL_QUIT:
            SDL_Quit();
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            // printf("%d %d\n", x, y);
            back = buttons2(x,y);
        default:
            break;
        }
        SDL_Rect box = {250+THEME*15, 150, 15,15};
        SDL_BlitSurface(select_image, NULL, screen, &box);
        //Update the surface
        SDL_UpdateWindowSurface( window );
    }

	return 0;
}

/**
 * \fn buttons
 * \author Baudean Paul <baudeanpau@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that handles the mouse input for the main menu screen
 *
 *
 * \param argc : 2
 * \param argv : the x coor of the mouse
 * \param argv : the y coor of the mouse
 * \return 0
 *
 * \remarks
 */

int buttons(int x,int y)
{
    if (x > 100 && x < 300){
        if (y > 74 && y < 123){
            reload_window(500, 400);
            play();
            reload_window(400, 400);
        }else if (y > 127 && y < 178){
            reload_window(500, 400);
            play_ai();
            reload_window(400, 400);
        }/*else if (y > 193 && y < 238){
        }*/else if (y > 266 && y < 313){
            setting();
        }else if (y > 328 && y < 373){
            printf("quit\n");
            SDL_Quit();
        }
    }
    return 0;
}

/**
 * \fn menu
 * \author Baudean Paul <baudeanpau@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief a function that displays and handles the main menu
 *
 *
 * \param argc : 0
 * \return 0
 *
 * \remarks
 */

int menu()
{
    //Fill the surface white
    //SDL_FillRect( screen, NULL, SDL_MapRGB( screen->format, 0xFF, 0xFF, 0xFF ) );

    SDL_Event event;
    int quit = 0;
    int x,y;

    while (!quit){

        SDL_WaitEvent(&event);
        SDL_BlitSurface( menu_image, NULL, screen, NULL );

        switch (event.type){
        case SDL_QUIT:
            quit = 1;
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState(&x, &y);
            buttons(x,y);
        default:
            break;
        }
        //Update the surface
        SDL_UpdateWindowSurface( window );
    }

	//Destroy window
	SDL_DestroyWindow( window );

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}

/**
 * \fn main
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 23/05/2020
 *
 * \brief the main function of the program
 *
 *
 * \param argc : 2
 * \param argc : argc
 * \param argv : argv
 * \return 0
 *
 * \remarks on windows SDL needs to include argc and argv to compile but on linux these are not nessasary
 */

int main( int argv, char* argc[] )
{
    if (argv){
        printf("%s\n", argc[argv-1]);
        // stop warnings on Linux -Wunused-parameter needed for compiling on CodeBlocks
    }
    srand(time(0));
    init();
    menu();
    return 0;
}
