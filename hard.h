#ifndef __HARD_HEADER__
#define __HARD_HEADER__

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/**
 * \fn get_hard_move
 * \author Grice Samson <gricemagic@eisti.eu>
 * \version 0.1
 * \date 25/04/2020
 *
 * \brief a function that determines the hardest position for the random number to appear.
 *
 *
 * \param argc : 2
 * \param argv : the table to add the number
 * \param argv : the move that you play
 * \return the table with the added number
 *
 * \remarks
 */

int ** get_hard_move(int ** p, int move);

#endif
